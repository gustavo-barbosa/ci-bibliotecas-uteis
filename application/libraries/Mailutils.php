<?php

class Mailutils
{
    protected $host = ''; // Servidor SMTP
    protected $user = ''; // Usuário SMTP
    protected $pass = ''; // Senha SMTP
    protected $port = ''; // Porta SMTP

    function __construct()
    {
        require_once("phpmailer/class.phpmailer.php");
    }

    public function sendMail($para, $mensagem, $titulo, $texto = '', $cc = NULL, $reponder = NULL)
    {

        // Inicia a classe PHPMailer
        $mail = new PHPMailer(true);
        // Define os dados do servidor e tipo de conexão
        // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        // $mail->IsSMTP(); // Define que a mensagem será SMTP
        try {
            $mail->isSMTP(); // Para testes
            $mail->Host = $this->host; // Endereço do servidor SMTP (Autenticação, utilize o host smtp.seudomínio.com.br)
            $mail->SMTPAuth = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
            $mail->Port = $this->port; //  Usar 587 porta SMTP
            $mail->Username = $this->user; // Usuário do servidor SMTP (endereço de email)
            $mail->Password = $this->pass; // Senha do servidor SMTP (senha do email usado)
            $mail->CharSet = 'UTF-8';
            //Define o remetente
            // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
            $mail->SetFrom($this->user); //Seu e-mail
            if (isset($responder)) {
                $mail->AddReplyTo($responder); //Seu e-mail
            }
            $mail->Subject = $titulo;//Assunto do e-mail

            //Define os destinatário(s)
            //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
            $mail->AddAddress($para);
            if (isset($cc)) {
                foreach ($cc as $contato) {
                    $mail->AddCC($contato);
                }
            }

            //Define o corpo do email
            $mail->MsgHTML($texto);

            if ($mail->Send()) {
                return true;
            }
        } catch (Exception $e) {
            print_r($e);
        }
    }
}