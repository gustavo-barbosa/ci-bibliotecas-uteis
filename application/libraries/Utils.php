<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Utils
{
    static public function onlyNumbers($number)
    {
        return preg_replace('/[^0-9]/', '', $number);
    }

    static public function checkCPF($cpf = null)
    {
        if (empty($cpf)) {
            return false;
        }
        $cpf = self::onlyNumbers($cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
        if (strlen($cpf) != 11 ||
            $cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999'
        ) {
            return false;
        }
        for ($verifier = 9; $verifier < 11; $verifier++) {
            for ($digit = 0, $position = 0; $position < $verifier; $position++) {
                $digit += $cpf{$position} * (($verifier + 1) - $position);
            }
            $digit = ((10 * $digit) % 11) % 10;
            if ($cpf{$position} != $digit) {
                return false;
            }
        }
        return true;
    }

    static public function validDate($data)
    {
        $data = explode('-', $data);

        return $data[2] . '/' . $data[1] . '/' . $data[0];
    }

    static public function base62($data)
    {
        $outstring = '';
        $l = strlen($data);
        for ($i = 0; $i < $l; $i += 8) {
            $chunk = substr($data, $i, 8);
            $outlen = ceil((strlen($chunk) * 8) / 6); //8bit/char in, 6bits/char out, round up
            $x = bin2hex($chunk);  //gmp won't convert from binary, so go via hex
            $w = gmp_strval(gmp_init(ltrim($x, '0'), 16), 62); //gmp doesn't like leading 0s
            $pad = str_pad($w, $outlen, '0', STR_PAD_LEFT);
            $outstring .= $pad;
        }
        return substr($outstring, -1) . $outstring;
    }

    static public function unbase62($data)
    {
        $data = substr($data, 1);
        $outstring = '';
        $l = strlen($data);
        for ($i = 0; $i < $l; $i += 11) {
            $chunk = substr($data, $i, 11);
            $outlen = floor((strlen($chunk) * 6) / 8); //6bit/char in, 8bits/char out, round down
            $y = gmp_strval(gmp_init(ltrim($chunk, '0'), 62), 16); //gmp doesn't like leading 0s
            $pad = str_pad($y, $outlen * 2, '0', STR_PAD_LEFT); //double output length as as we're going via hex (4bits/char)
            $outstring .= pack('H*', $pad); //same as hex2bin
        }
        return $outstring;
    }

    static public function checkEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return false;
        }
    }

    static public function formatValue($valor)
    {
        $valor = str_replace('.', '', $valor);
        $valor = str_replace(',', '.', $valor);
        return $valor;
    }

}