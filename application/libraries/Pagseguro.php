<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pagseguro
{
    protected $post = array();
    protected $amount = 0;

    /**
     * @var string
     */
    private static $notificationUrl = "";
    private static $email = "";
    private static $tokenSandbox = "";
    private static $tokenProduction = "";
    private static $sandbox = false;


    /**
     * @return string
     */
    public static function getEmail()
    {
        return self::$email;
    }

    /**
     * @return string
     */
    public static function getToken()
    {
        return self::isSandbox() ? self::$tokenSandbox : self::$tokenProduction;
    }

    /**
     * @return string
     */
    public static function getNotificationUrl()
    {
        return self::$notificationUrl;
    }

    /**
     * @return bool
     */
    public static function isSandbox()
    {
        return self::$sandbox;
    }

    /**
     *
     */
    public static function setSandbox()
    {
        self::$sandbox = true;
    }

    /**
     *
     */
    public static function setProduction()
    {
        self::$sandbox = false;
    }

    public static function setAccountCredentials($email, $token, $isSandbox = true)
    {
        self::$email = $email;

        if ($isSandbox === true) {
            self::setSandbox();
            self::$tokenSandbox = $token;
        } else {
            self::setProduction();
            self::$tokenProduction = $token;
        }
    }

    /* Captura token de autorização */
    public function captureToken()
    {
        if ($this->isSandbox()) {
            $url = 'https://ws.sandbox.pagseguro.uol.com.br/v2/sessions/';
        } else {
            $url = 'https://ws.pagseguro.uol.com.br/v2/sessions/';
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
            'email' => $this->getEmail(),
            'token' => $this->getToken()
        )));

        $response = curl_exec($ch);

        curl_close($ch);

        $_SESSION['xml'] = $url;

        libxml_use_internal_errors(true);

        $xml = simplexml_load_string($response);

        if ($xml) {
            return $xml->id;
        } else {
            return $response;
        }
    }

    public function getJSLink()
    {
        $data = new stdClass;
        if ($this->isSandbox()) {
            $data->script = 'https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js';
        } else {
            $data->script = 'https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js';
        }

        return $data;
    }

    public function setCustomerCPF($data)
    {
        $CI =& get_instance();

        $CI->load->library('utils');
        $data = $CI->utils->onlyNumbers($data);

        if (!$CI->utils->checkCPF($data)) {
            //PagSeguro error code 1114
            throw new InvalidArgumentException('CPF Inválido: ' . $data);
        }

        $this->post['senderCPF'] = $data;
    }

    public function setCustomerCNPJ($data)
    {
        $CI =& get_instance();

        $CI->load->library('utils');
        $data = $CI->utils->onlyNumbers($data);

        if (strlen($data) !== 14) {
            //PagSeguro error code 1115
            throw new InvalidArgumentException('CNPJ Inválido: ' . $data);
        }
        $this->post['senderCNPJ'] = $data;
    }

    public function setCustomerName($data)
    {
        if (strlen($data) > 50) {
            //PagSeguro error code 1121
            throw new InvalidArgumentException('Nome pode conter, no máximo, 50 caracteres: ' . $data);
        }
        $this->post['senderName'] = $data;
    }

    public function setCustomerEmail($data)
    {
        if (!filter_var($data, FILTER_VALIDATE_EMAIL)) {
            //PagSeguro error code 1132
            throw new InvalidArgumentException('E-mail inválido: ' . $data);
        }
        if (strlen($data) > 60) {
            //PagSeguro error code 1131
            throw new InvalidArgumentException('E-mail deve conter até 60 caracteres: ' . $data);
        }
        $this->post['senderEmail'] = $data;
    }

    public function setCustomerPhone($areaCode, $number)
    {
        $CI =& get_instance();

        $CI->load->library('utils');
        $areaCode = $CI->utils->onlyNumbers($areaCode);
        $areaCode = substr($areaCode, 0, 2);

        $number = $CI->utils->onlyNumbers($number);
        $number = substr($number, 0, 9);

        if (strlen($areaCode) !== 2) {
            //PagSeguro error code 1151
            throw new InvalidArgumentException('Código de área inválido: ' . $areaCode);
        }

        if (strlen($number) < 8 || strlen($number) > 9) {
            //PagSeguro error code 1161
            throw new InvalidArgumentException('Número de telefone inválido: ' . $number);
        }

        $this->post['senderAreaCode'] = $areaCode;
        $this->post['senderPhone'] = $number;
        $this->post['creditCardHolderAreaCode'] = $areaCode;
        $this->post['creditCardHolderPhone'] = $number;
    }

    public function setReference($data)
    {
        if (strlen($data) > 200) {
            //PagSeguro error code 1001
            throw new InvalidArgumentException('Tamanho máximo da referência é de 200 caracteres: ' . $data);
        }
        $this->post['reference'] = $data;
    }

    public function setProductsInfo($products)
    {
        $count = 1;
        foreach ($products as $product) {
            $this->post['itemId' . $count] = $product['id'];
            $this->post['itemDescription' . $count] = $product['description'];
            $this->post['itemAmount' . $count] = number_format($product['price'], 2, '.', '');
            $this->post['itemQuantity' . $count] = $product['qtd'];
            $this->amount += $product['qtd'] * $this->post['itemAmount' . $count];
            $count++;
        }
    }

    public function setNoFeeInstallment($qtd)
    {
        $this->post['installmentQuantity'] = $qtd;
        $this->post['installmentValue'] = number_format($this->amount / $qtd, 2, '.', '');
    }

    public function setInstallment($qtd, $value)
    {
        $this->post['installmentQuantity'] = $qtd;
        $this->post['installmentValue'] = number_format($value, 2, '.', '');
    }

    public function setSenderHash($data)
    {
        $this->post['senderHash'] = $data;
    }

    public function setCreditCardToken($data)
    {
        $this->post['creditCardToken'] = $data;
    }

    public function setCreditCardHolderName($data)
    {
        if (strlen($data) > 50) {
            //PagSeguro error code 1121
            throw new InvalidArgumentException('Nome no cartão deve conter, no máximo, 50 caracteres: ' . $data);
        }
        $this->post['creditCardHolderName'] = $data;
    }

    public function setCreditCardHolderCPF($data)
    {
        $CI =& get_instance();

        $CI->load->library('utils');
        $data = $CI->utils->onlyNumbers($data);

        if (!Utils::checkCPF($data)) {
            //PagSeguro error code 1114
            throw new InvalidArgumentException('CPF do dono do cartão inválido: ' . $data);
        }

        $this->post['creditCardHolderCPF'] = $data;
    }

    public function setCreditCardHolderBirthDate($data)
    {
        $CI =& get_instance();

        $CI->load->library('utils');
        if (!$CI->utils->validDate($data)) {
            throw new InvalidArgumentException('Data de aniversário inválida: ' . $data);
        }
        $this->post['creditCardHolderBirthDate'] = $CI->utils->validDate($data);
    }

    public function setPaymentMethod($data)
    {
        if ($data != 'creditCard' && $data != 'boleto') {
            throw new InvalidArgumentException($data . ' não é um meio de pagamento válido');
        } else {
            $this->post['paymentMethod'] = $data;
        }
    }

    public function setNotificationURL($data)
    {
        if (strlen($data) > 255 || !filter_var($data, FILTER_VALIDATE_URL)) {
            //PagSeguro error code 1070
            //erro 500 when dont use http and is close to maximum size
            throw new InvalidArgumentException('URL de notificação inválida (max: 255 caracteres): ' . $data);
        }
        $this->post['notificationURL'] = $data;
    }

    public function setCustomerAddressStreet($data)
    {
        $this->post['shippingAddressStreet'] = $data;
        $this->post['billingAddressStreet'] = $data;
    }

    public function setCustomerAddressNumber($data)
    {
        $this->post['shippingAddressNumber'] = $data;
        $this->post['billingAddressNumber'] = $data;
    }

    public function setCustomerAddressDistrict($data)
    {
        $this->post['shippingAddressDistrict'] = $data;
        $this->post['billingAddressDistrict'] = $data;
    }

    public function setCustomerAddressPostalCode($data)
    {
        $data = Utils::onlyNumbers($data);
        $this->post['shippingAddressPostalCode'] = $data;
        $this->post['billingAddressPostalCode'] = $data;
    }

    public function setCustomerAddressCity($data)
    {
        $this->post['shippingAddressCity'] = $data;
        $this->post['billingAddressCity'] = $data;
    }

    public function setCustomerAddressState($data)
    {
        $data = strtoupper($data);
        if (strlen($data) === 2) {
            $this->post['shippingAddressState'] = $data;
            $this->post['billingAddressState'] = $data;
        }
    }

    protected function requiredFields()
    {
        if (!isset($this->post['senderCPF']) && !isset($this->post['senderCNPJ'])) {
            //PagSeguro error code 1110
            throw new Exception('CPF ou CNPJ é obrigatório');
        }
        if (!isset($this->post['senderName'])) {
            //PagSeguro error code 1120
            throw new Exception('Nome é obrigatório');
        }
        if (!isset($this->post['senderEmail'])) {
            //PagSeguro error code 1130
            throw new Exception('E-mail é obrigatório');
        }
        if (!isset($this->post['senderPhone'])) {
            //PagSeguro error code 1140
            throw new Exception('Telefone é obrigatório');
        }
    }

    protected function requiredFieldsButNot()
    {
        if (!isset($this->post['paymentMode'])) {
            $this->post['paymentMode'] = 'default';
        }

        if (!isset($this->post['currency'])) {
            $this->post['currency'] = 'BRL';
        }

        if (!isset($this->post['paymentMethod'])) {
            $this->post['paymentMethod'] = 'creditCard';
        }

        if (!isset($this->post['reference'])) {
            $this->post['reference'] = 'generated automatically in: ' . date('r');
        }

        if (!isset($this->post['noInterestInstallmentQuantity']) && $this->post['paymentMethod'] == 'creditCard') {
            $this->post['noInterestInstallmentQuantity'] = 2;
        }

        if (!isset($this->post['notificationURL'])) {
            $this->post['notificationURL'] = $this->getNotificationUrl();
        }

        if (!isset($this->post['email'])) {
            $this->post['email'] = $this->getEmail();
            $this->post['receiverEmail'] = $this->getEmail();
            $this->post['token'] = $this->getToken();
        }

        if (!isset($this->post['extraAmount'])) {
            $this->post['extraAmount'] = '0.00';
        }

        if (!isset($this->post['billingAddressCountry'])) {
            $this->post['billingAddressCountry'] = 'BRA';
            $this->post['shippingAddressCountry'] = 'BRA';
        }

        if (!isset($this->post['shippingType'])) {
            $this->post['shippingType'] = '3';
            $this->post['shippingCost'] = '0.00';
        }
    }

    public function status($status)
    {
        $array = [
            1 => "Ag. Pagamento",
            2 => "Em análise",
            3 => "Paga",
            4 => "Concluída",
            5 => "Em disputa",
            6 => "Devolvida",
            7 => "Erro no Pagamento"
        ];

        return $array($status);
    }

    public function getNotification($notificationCode)
    {
        if ($this->isSandbox()) {
            $url = 'https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/notifications/';
        } else {
            $url = 'https://ws.pagseguro.uol.com.br/v2/transactions/notifications/';
        }

        $url = $url . $notificationCode . '?email=' . $this->getEmail() . '&token=' . $this->getToken();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        libxml_use_internal_errors(true);

        $xml = simplexml_load_string($response);

        if ($xml) {
            return $xml;
        } else {
            return $response;
        }
    }


    /* Envia os dados da transação para o pagseguro  */
    public function send()
    {
        $this->requiredFields();
        $this->requiredFieldsButNot();

        if ($this->isSandbox()) {
            $url = 'https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/';
        } else {
            $url = 'https://ws.pagseguro.uol.com.br/v2/transactions/';
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->post));

        $response = curl_exec($ch);

        curl_close($ch);

        $xml = simplexml_load_string($response);

        $_SESSION['pagseguro_data'] = http_build_query($this->post);
        $_SESSION['pagseguro_response'] = $response;

        if (isset($xml->error)) {
            $json = array(
                'error' => $xml->error
            );
        } else {
            $json = $xml;
        }

        return $json;
    }
}